import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Simple Intrest Calc",
        home: SimpleInterestCalculatorForm(),
        theme: ThemeData(
            primaryColor: Colors.indigo,
            accentColor: Colors.indigo,
            brightness: Brightness.dark),
      ),
    );

class SimpleInterestCalculatorForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SimpleInterestCalculatorFormState();
  }
}

class _SimpleInterestCalculatorFormState
    extends State<SimpleInterestCalculatorForm> {
  var _currencies = ["Rupees", "Pounds", "Dollars", "Others"];
  final _minMargin = 5.0;
  var displayResult = "";
  var _formKey = GlobalKey<FormState>();

  String _currentSelectedCurrency = '';
  TextEditingController principleController = TextEditingController();
  TextEditingController rateOfInterestController = TextEditingController();
  TextEditingController termController = TextEditingController();

  initState() {
    super.initState();
    _currentSelectedCurrency = _currencies[1];
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return Scaffold(
        appBar: AppBar(
          title: Text("Simple Intrest Calculator"),
        ),
        body: Form(
          key: _formKey,
          child: Padding(
              padding: EdgeInsets.all(_minMargin * 2),
              child: ListView(
                children: <Widget>[
                  getCalculatorAssetImage(),
                  Padding(
                    padding:
                    EdgeInsets.only(top: _minMargin, bottom: _minMargin),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      style: textStyle,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return "Please enter valid principal amount.";
                        }
                      },
                      controller: principleController,
                      decoration: InputDecoration(
                          labelText: "Principle",
                          hintText: "Enter principle amount e.g., 1800",
                          labelStyle: textStyle,
                          errorStyle: TextStyle(
                              color: Colors.yellowAccent, fontSize: 15.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0))),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: _minMargin, bottom: _minMargin),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      style: textStyle,
                      controller: rateOfInterestController,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return "Please enter valid rate of intrest.";
                        }
                      },
                      decoration: InputDecoration(
                          labelText: "Rate Of Interest",
                          hintText: "Enter rate of interest e.g., 1",
                          labelStyle: textStyle,
                          errorStyle: TextStyle(
                              color: Colors.yellowAccent, fontSize: 15.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0))),
                    ),
                  ),
                  Padding(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            style: textStyle,
                            controller: termController,
                            keyboardType: TextInputType.number,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return "Please enter valid term.";
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Term",
                                hintText: "Time in years",
                                labelStyle: textStyle,
                                errorStyle: TextStyle(
                                    color: Colors.yellowAccent, fontSize: 15.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0))),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _minMargin * 2),
                        ),
                        Expanded(
                          child: DropdownButton<String>(
                            items: _currencies.map((String selectedValue) {
                              return DropdownMenuItem<String>(
                                value: selectedValue,
                                child: Text(selectedValue),
                              );
                            }).toList(),
                            onChanged: (String selectedValue) {
                              setState(() {
                                _currentSelectedCurrency = selectedValue;
                              });
                            },
                            value: _currentSelectedCurrency,
                          ),
                        )
                      ],
                    ),
                    padding: EdgeInsets.only(
                        top: _minMargin * 2, bottom: _minMargin * 2),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: _minMargin, bottom: _minMargin),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            color: Theme.of(context).accentColor,
                            textColor: Theme.of(context).primaryColorDark,
                            child: Text(
                              "Calculate",
                              textScaleFactor: 1.5,
                            ),
                            onPressed: () {
                              setState(() {
                                if (_formKey.currentState.validate()) {
                                  displayResult =
                                      onCalculateTotalPayableAmount();
                                } else {
                                  displayResult = "";
                                }
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: RaisedButton(
                            color: Theme.of(context).primaryColorDark,
                            textColor: Theme.of(context).primaryColorLight,
                            child: Text("Reset", textScaleFactor: 1.5),
                            onPressed: () {
                              setState(() {
                                resetForm();
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(_minMargin * 2),
                    child: Center(
                        child: Text(
                      displayResult,
                      style: textStyle,
                    )),
                  )
                ],
              )),
        ));
  }

  Widget getCalculatorAssetImage() {
    AssetImage assetImage = AssetImage('images/calculator.jpeg');
    Image calculatorImageIcon = Image(
      image: assetImage,
      width: 125.0,
      height: 125.0,
    );
    return Container(
        margin: EdgeInsets.all(_minMargin * 10),
        child: Center(child: calculatorImageIcon));
  }

  String onCalculateTotalPayableAmount() {
    double principle = double.parse(principleController.text);
    double rateOfInterest = double.parse(rateOfInterestController.text);
    double term = double.parse(termController.text);
    double totalAmountPayable =
        principle + (principle * rateOfInterest * term) / 100;

    return "After $term years your invenstment will be worth  $totalAmountPayable $_currentSelectedCurrency";
  }

  void resetForm() {
    principleController.clear();
    rateOfInterestController.clear();
    termController.clear();
    displayResult = "";
    _currentSelectedCurrency = _currencies[1];
  }
}
