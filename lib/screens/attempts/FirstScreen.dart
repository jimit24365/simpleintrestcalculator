import 'dart:math';

import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.blueAccent,
        child: Center(
          child: Text(
            generateLuckyNumber(),
            textDirection: TextDirection.ltr,
            style: TextStyle(color: Colors.amber, fontSize: 32),
          ),
        ));
  }

  String generateLuckyNumber() {
    var random = Random();
    var luckyNumber = random.nextInt(10);
    return "Your Lucky Number is $luckyNumber";
  }
}
