import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreenWithBottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Section 2",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Basic List View"),
        ),
        body: getDataSourcedListView(),
        bottomNavigationBar: BottomNavigationBar(currentIndex: 1, items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text("Items"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.build),
            title: Text("Settings"),
          ),
        ]),
        floatingActionButton: Builder(
          builder: (BuildContext context) {
            return FloatingActionButton(
                child: Icon(Icons.add),
                tooltip: 'Add One More Item',
                onPressed: () {
                  debugPrint("FAB button clicked");
                  showSnackBar(context, "FAB button clicked");
                });
          },
        ),
      ),
    );
  }

  getListView() {
    var listView = ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.landscape),
          title: Text("Landscape"),
          subtitle: Text("Beautiful view"),
          trailing: Icon(Icons.wb_sunny),
          onTap: () {
            debugPrint("Hello World");
          },
        ),
        ListTile(
          leading: Icon(Icons.desktop_windows),
          title: Text("Windows"),
        ),
        ListTile(
          leading: Icon(Icons.desktop_mac),
          title: Text("Mac"),
        )
      ],
    );
    return listView;
  }

  showSnackBar(BuildContext context, String item) {
    var snackBar = SnackBar(
      content: Text("Hello New Item $item"),
      duration: Duration(milliseconds: 1000),
      action: SnackBarAction(
        label: "Dummy Action",
        onPressed: () {
          debugPrint("Dummy operation performed");
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  List<String> getListData() {
    return List<String>.generate(1000, (counter) => "Item $counter");
  }

  getDataSourcedListView() {
    var listItems = getListData();

    return ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (
          context,
          index,
        ) {
          return ListTile(
            leading: Icon(Icons.add),
            title: Text(listItems[index]),
            onTap: () {
              debugPrint('${listItems[index]} was tapped');
              showSnackBar(context, listItems[index]);
            },
          );
        });
  }
}
