import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
        child: Container(
      alignment: Alignment.center,
      color: Colors.greenAccent,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "Hello Duniya",
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      fontSize: 30,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.w300,
                      color: Colors.white),
                ),
              ),
              Expanded(
                child: Text(
                  "App kesen ho",
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      fontSize: 20,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.w700,
                      color: Colors.white),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "Hello Duniya",
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      fontSize: 30,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.w300,
                      color: Colors.white),
                ),
              ),
              Expanded(
                child: Text(
                  "App kesen ho",
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      fontSize: 20,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.w700,
                      color: Colors.white),
                ),
              ),
            ],
          ),
          MyImage(),
          ShowWorldButton(),
        ],
      ),
    ));
  }
}

class MyImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage worldImageAsset = AssetImage('images/w1.jpeg');
    Image worldImage = Image(
      image: worldImageAsset,
    );
    return Container(
      child: worldImage,
    );
  }
}

class ShowWorldButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30.0),
      width: 300,
      height: 50,
      child: RaisedButton(
        color: Colors.deepOrange,
        child: Text(
          "Show Your World",
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w700),
        ),
        elevation: 6.0,
        onPressed: () => showWorld(context),
      ),
    );
  }

  void showWorld(BuildContext context) {
    var alertDiaog = AlertDialog(
      title: Text("World is diying"),
      content: Text("Go to new world"),
    );
    showDialog(context: context, builder: (BuildContext context) => alertDiaog);
  }
}
