import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavoriteCity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FavouriteCityState();
  }
}

class FavouriteCityState extends State<FavoriteCity> {
  String _name = "";
  var _currencies = ["Rupee", "Dollar", "Pound", "Other"];

  String _currentSelectedCurrency = "Rupee";

  @override
  Widget build(BuildContext context) {
    debugPrint("Build of FavouriteCityState is called.");
    return Scaffold(
        appBar: AppBar(
          title: Text("Favourite City"),
        ),
        body: Container(
          margin: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              TextField(
                onChanged: (String cityName) {
                  setState(() {
                    debugPrint("setState is called.");
                    _name = cityName;
                  });
                },
              ),
              DropdownButton<String>(
                items: _currencies.map((String dropDownStringItem) {
                  return DropdownMenuItem<String>(
                    value: dropDownStringItem,
                    child: Text(dropDownStringItem),
                  );
                }).toList(),
                onChanged: (String selectedValue) {
                  _onCurrencyOptionChanged(selectedValue);
                },
                value: _currentSelectedCurrency,
              ),
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("Your favorite city is $_name"))
            ],
          ),
        ));
  }

  void _onCurrencyOptionChanged(String selectedValue) {
    setState(() {
      _currentSelectedCurrency = selectedValue;
    });
  }
}
